# prep
make genfile

# run docker-compose.yml
podman-compose up

# generate requests on caching proxies
make genreq

# query stats
| service | url                    |
| ------- | ---------------------- |
| grafana | http://localhost:3000/ |

LogQL pattern labeling on combined logs:
(works in grafana/loki - but not the match selector in promtail)
```
<ip> - - <_> "<method> <path> <_>" <status> <size> "<_>" "<_>" "<_>"
```
Use this with "Ad-hoc stats" UI function to see path request amount / overall percentage

![logql](./example/logql.png)

# diagram

![diagram](./example/struct.svg)
