SHELL=/usr/bin/env bash
MAKEFLAGS += --silent

.EXPORT_ALL_VARIABLES:
# enable "set -x" in scripts
TRACE=0

.PHONY: all

all: help

# make target to show target description
help:
	@grep -B1 -E "^[a-zA-Z0-9_-]+\:([^\=]|$$)" Makefile \
	| grep -v -- -- \
	| sed 'N;s/\n/###/' \
	| sed -n 's/^#: \(.*\)###\(.*\):.*/\2###\1/p' \
	| column -t  -s '###'

#: - podman up
up:
	podman-compose up

#: - generate sample files
genfile:
	dd bs=1M count=1 if=/dev/zero of=./origin/pub/player.exe
	dd bs=1M count=1 if=/dev/zero of=./origin/pub/studio.exe
	dd bs=1M count=1 if=/dev/zero of=./origin/pub/designer.exe

#: - generate requests
genreq:
	curl -X GET http://localhost:8081/player.exe -o /dev/null
	curl -X GET http://localhost:8082/player.exe -o /dev/null
	curl -X GET http://localhost:8081/studio.exe -o /dev/null
	curl -X GET http://localhost:8082/studio.exe -o /dev/null
	curl -X GET http://localhost:8081/designer.exe -o /dev/null
	curl -X GET http://localhost:8082/designer.exe -o /dev/null
